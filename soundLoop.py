#!/usr/bin/env python3

import tkinter as tk
from subprocess import call;

def enableLoop():
    call(["pactl","load-module","module-loopback"]);

def disableLoop():
    call(["pactl","unload-module","module-loopback"]);

root = tk.Tk();
frame = tk.Frame(root);
frame.pack();

windowWidth = root.winfo_reqwidth();
windowHeight = root.winfo_reqheight();

positionRight = int(root.winfo_screenwidth()/2 - windowWidth/2)
positionDown = int(root.winfo_screenheight()/2 - windowHeight/2)

root.geometry("+{}+{}".format(positionRight, positionDown))

buttonEnable = tk.Button(frame, text="Enable", command=enableLoop);
buttonDisable = tk.Button(frame, text="Disable", command=disableLoop);

buttonEnable.pack(side=tk.LEFT);
buttonDisable.pack(side=tk.RIGHT);
root.mainloop();
