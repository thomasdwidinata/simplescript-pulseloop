# Simple Script - Pulse Loop

## PulseAudio Loopback Module Enabler/Disabler 

### Description
A simple script made for enabling and disabling loopback module on PulseAudio without having to write `pactl` command to enable or disable the module.

### Technologies
* Python 3
* `Tkinter` Library for Python
* PulseAudio (Uses `pactl` command)
